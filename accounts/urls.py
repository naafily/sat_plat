from django.urls import path
from .views import AdminUserLoginAPIView, GeneralUserLoginAPIView

urlpatterns = [
    path('admins/login/', AdminUserLoginAPIView.as_view()),
    path('users/login/', GeneralUserLoginAPIView.as_view()),
]