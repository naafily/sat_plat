from django.contrib import admin
from .models import AdminUser, GeneralUser

admin.site.register(AdminUser)
admin.site.register(GeneralUser)