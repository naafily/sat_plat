from rest_framework import serializers

from .models import AdminUser, GeneralUser


class AdminUserLoginSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(source='user.id', write_only=True,
                                    required=True)
    user_password = serializers.CharField(source='user.password', write_only=True,
                                          required=True)

    class Meta:
        model = AdminUser
        fields = ('user_id', 'user_password')


class GeneralUserLoginSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(source='user.id', write_only=True,
                                    required=True)
    user_password = serializers.CharField(source='user.password', write_only=True,
                                          required=True)

    class Meta:
        model = GeneralUser
        fields = ('user_id', 'user_password')


