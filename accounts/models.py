from django.contrib.auth.models import User
from django.db import models


class AdminUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='admin_user')


class GeneralUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='general_user')

