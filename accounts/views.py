from django.contrib.auth import authenticate, login
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import AdminUser, GeneralUser
from .serializers import AdminUserLoginSerializer, GeneralUserLoginSerializer


class BaseLoginAPIView(APIView):
    def _get_model_class(self):
        raise NotImplementedError()

    def post(self, request):
        user_id = request.data.get('user_id')
        password = request.data.get('user_password')
        model = self._get_model_class()
        obj = get_object_or_404(model, user_id=user_id)
        user = obj.user
        if user is not None:
            authenticated_user = authenticate(username=user.username, password=password)
            if authenticated_user is not None:
                login(request, user)
                return Response(status=status.HTTP_200_OK)

        return Response(status=status.HTTP_404_NOT_FOUND)


class AdminUserLoginAPIView(BaseLoginAPIView):
    serializer_class = AdminUserLoginSerializer

    def _get_model_class(self):
        return AdminUser


class GeneralUserLoginAPIView(BaseLoginAPIView):
    serializer_class = GeneralUserLoginSerializer

    def _get_model_class(self):
        return GeneralUser


