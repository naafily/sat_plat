from rest_framework import permissions

from accounts.models import AdminUser, GeneralUser


class UserPermission(permissions.BasePermission):

    def __init__(self, allowed_methods):
        super().__init__()
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return (GeneralUser.objects.filter(
                user=request.user).exists() and request.method in self.allowed_methods) or AdminUser.objects.filter(
                user=request.user).exists()
