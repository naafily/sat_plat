from django.urls import path

from .views import UploadFileView, DataView, DataList, TimeSeriesChartView

urlpatterns = [
    path('upload/', UploadFileView.as_view()),
    path('data/', DataList.as_view()),
    path('data/<int:pk>', DataView.as_view()),
    path('chart/', TimeSeriesChartView.as_view()),
]
