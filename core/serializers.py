from rest_framework import serializers

from .models import CSVFile, Data


class CSVFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSVFile
        fields = ("file", "description", "uploaded_at")


class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = "__all__"
        read_only_fields = ('file',)

