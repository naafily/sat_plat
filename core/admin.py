from django.contrib import admin
from .models import Data, CSVFile

admin.site.register(Data)
admin.site.register(CSVFile)
