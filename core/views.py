import io
from functools import partial

import matplotlib.pyplot as plt
import pandas as pd
from django.core.files.base import File
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Data
from .permissions import UserPermission
from .serializers import CSVFileSerializer, DataSerializer


class UploadFileView(generics.CreateAPIView):
    serializer_class = CSVFileSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file = serializer.validated_data['file']
        obj = serializer.save(file=File(file).name)
        reader = pd.read_csv(file)
        for _, row in reader.iterrows():
            Data.objects.create(the_json=row.to_json(), file=obj)
        return Response({"status": "success"},
                        status.HTTP_201_CREATED)


class DataView(APIView):
    serializer_class = DataSerializer
    permission_classes = (partial(UserPermission, ['GET', 'HEAD']),)

    def get_object(self, pk):
        return get_object_or_404(Data, id=pk)

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        data = self.get_object(pk)
        serializer = DataSerializer(data)
        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        data = self.get_object(pk)
        serializer = DataSerializer(data, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DataList(generics.ListAPIView):
    queryset = Data.objects.all()
    serializer_class = DataSerializer


class TimeSeriesChartView(APIView):
    def get(self, request, *args, **kwargs):
        objects = Data.objects.all()
        label = []
        value = []
        for i in objects:
            if 'Date\n(Persian)' in i.dic.keys():
                label.append(i.dic['Date\n(Persian)'])
                value.append(i.dic['Water Level\n(m above sea level)'])
        time_series_data = {'label': label, 'value': value}

        dataframe = pd.DataFrame(
            time_series_data, columns=[
                'label', 'value'])
        dataframe = dataframe.set_index("label")
        plt.figure(figsize=[30, 6])
        dataframe = dataframe.dropna()
        plt.plot(dataframe["value"])
        figure = io.BytesIO()
        plt.savefig(figure, format="png")
        return HttpResponse(figure.getvalue(), content_type='image/gif')
