from django.db import models
import json


class CSVFile(models.Model):
    file = models.FileField(upload_to='files/', null=True)
    description = models.CharField(null=True, max_length=255)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.file.name


class Data(models.Model):
    the_json = models.JSONField(default=dict)
    file = models.ForeignKey(CSVFile, on_delete=models.CASCADE, null=True)

    @property
    def dic(self):
        data = json.loads(self.the_json)
        return data
